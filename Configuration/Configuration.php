<?php
return array(
	"DATABASE" => array(
		"HOSTNAME" => "localhost",
		"USERNAME" => "root",
		"PASSWORD" => "",
		"DATABASE" => "mvcdevelopment"
	),
	"GENERAL" => array(
		"HOMEPAGE" => array(
			"CONTROLLER" 	=> "PostsController",
			"ACTION"		=> "latestAction"
		)
	),
	"ADMINISTRATOR" => array(
		"PostsController::newAction",
		"PostsController::createAction"
	)
);