DROP DATABASE mvcdevelopment;

CREATE DATABASE mvcdevelopment;
USE mvcdevelopment;

CREATE TABLE categories (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	description TEXT,
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO categories (name) VALUES ('General'), ('Second');

CREATE TABLE posts (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	content TEXT,
	category INT(11),
	image VARCHAR(255),
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP,
	FOREIGN KEY (category) REFERENCES categories(id)
);

INSERT INTO posts (name,content,category) VALUES ('Lorem ipsum dolor sit amet','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ac blandit quam, non mollis odio. Nam fringilla velit id lorem bibendum, eget tristique velit dapibus. Quisque fermentum eleifend sodales. Duis placerat diam sit amet magna cursus, ac aliquam tortor aliquet. Ut cursus, orci sed fringilla rutrum, odio turpis placerat neque, et ultricies dui eros fermentum quam. Suspendisse et enim faucibus, vehicula felis ut, egestas arcu. Maecenas viverra mi in venenatis pharetra. Donec pellentesque, orci ac dictum aliquet, purus lectus malesuada erat, at malesuada ligula sem eget neque. Duis commodo ligula vel est efficitur fermentum. Vivamus lacus neque, dignissim auctor sapien a, vestibulum condimentum turpis. Nunc eleifend, nisl in dignissim placerat, dolor urna vehicula enim, non interdum urna tortor in arcu.',1);

CREATE TABLE users (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	administrator INT(11) DEFAULT 0,
	firstname VARCHAR(255),
	lastname VARCHAR(255),
	emailaddress VARCHAR(255) UNIQUE NOT NULL,
	password VARCHAR(255) NOT NULL,
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO users (administrator,emailaddress,password) VALUES (1, "admin","1234");

CREATE TABLE products (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	description TEXT,
	image VARCHAR(255),
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO products (name) VALUES ("Telefon mobil Apple iPhone 7, 32GB, Black"),("Telefon mobil Apple iPhone 7, 32GB, Rose Gold");