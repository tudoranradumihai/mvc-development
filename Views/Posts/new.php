<form method="POST" action="index.php?C=Posts&A=create" enctype="multipart/form-data">
	<div>
		<input type="text" name="name" placeholder="Post Name" />
	</div>
	<div>
		<textarea name="content"></textarea>
	</div>
	<div>
		<select name="category">
			<option value="">...</option>
			<?php foreach($categories as $category) { ?>
				<option value="<?php echo $category->getId() ?>"><?php echo $category->getName() ?></option>
			<?php } ?>
		</select>
	</div>
	<div>
		<input type="file" name="image" />
	</div>
	<div>
		<input type="submit">
	</div>
</form>