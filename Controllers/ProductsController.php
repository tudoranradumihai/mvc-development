<?php

Class ProductsController extends Controller {

	private $productsRepository;

	public function __construct(){
		$this->productsRepository = new ProductsRepository();
	}

	public function defaultAction(){
		
	}

	public function listAction(){
		$products = $this->productsRepository->findAll();
		include "Views/Products/list.php";
	}

	public function showAction(){
		if (array_key_exists("ID", $_GET)){
			$product = $this->productsRepository->findById($_GET["ID"]);
			include "Views/Products/show.php";
		} else {
			header("Location: index.php?C=Products&A=list");
		}
	}

}