<?php

Class PostsController extends Controller {

	private $postsRepository;

	public function __construct(){
		$this->postsRepository = new PostsRepository();
	}

	public function defaultAction(){
		self::listAction();
	}

	public function listAction(){
		$posts = $this->postsRepository->findAll();
		include "Views/Posts/list.php";
	}

	public function latestAction(){
		$posts = $this->postsRepository->findLastThree();
		include "Views/Posts/latest.php";
	}

	public function listByCategoryAction(){
		if (array_key_exists("ID", $_GET)){
			$categoriesRepository = new CategoriesRepository();
			$category = $categoriesRepository->findByID($_GET["ID"]);
			$posts = $this->postsRepository->findByCategory($_GET["ID"]);
			include "Views/Posts/listByCategory.php";
		} else {
			header("Location: index.php");
		}
	}

	public function newAction(){
		$categoriesRepository = new CategoriesRepository();
		$categories = $categoriesRepository->findAll();
		include "Views/Posts/new.php";
	}

		public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$post = new Posts();
			$post->setName($_POST["name"]);
			$post->setContent($_POST["content"]);
			$post->setCategory($_POST["category"]);
			if(array_key_exists("image", $_FILES)){
				if($_FILES["image"]["error"]==0){
					if(!file_exists("Resources")){
						mkdir("Resources");
					}
					if(!file_exists("Resources/Public")){
						mkdir("Resources/Public");
					}
					if(!file_exists("Resources/Public/Uploads")){
						mkdir("Resources/Public/Uploads");
					}
					$extension = explode(".",$_FILES["image"]["name"]);
					$extension = $extension[count($extension)-1];
					$filename = date("Ymd-Hds")."-".rand(10000,99999)."-".preg_replace("/[^A-Za-z0-9?!]/",'',str_replace(" ","-",strtolower($_POST["name"]))).".".$extension;
					move_uploaded_file($_FILES["image"]["tmp_name"], "Resources/Public/Uploads/".$filename);
					$post->setImage($filename);
				}
			}
			$this->postsRepository->insert($post);
			header("Location: index.php?C=Posts&A=list");
		} else {
			header("Location: index.php?C=Posts&A=new");
		}
	}

}