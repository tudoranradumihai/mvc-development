<?php

Class CartController extends Controller {

	public function defaultAction(){

	}

	public function addAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			if (!array_key_exists("products", $_SESSION)){
				$_SESSION["products"] = array();
			}
			if(array_key_exists($_POST["product"], $_SESSION["products"])){
				$_SESSION["products"][$_POST["product"]] += intval($_POST["quantity"]);
			} else {
				$_SESSION["products"][$_POST["product"]] = intval($_POST["quantity"]);
			}
		} 			
		header("Location: index.php?C=Cart&A=show");
	}

	public function showAction(){
		if(array_key_exists("products", $_SESSION)){
			$products = $_SESSION["products"];
		}
		var_dump($products);
	}

}