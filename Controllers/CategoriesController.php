<?php

Class CategoriesController extends Controller {

	private $categoriesRepository;

	public function __construct(){
		$this->categoriesRepository = new CategoriesRepository();
	}

	public function defaultAction(){
		self::listAction();
	}

	public function listAction(){
		$categories = $this->categoriesRepository->findAll();
		include "Views/Categories/list.php";
	}

	public function newAction(){
		include "Views/Categories/new.php";
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$category = new Categories();
			$category->setName($_POST["name"]);
			$category->setDescription($_POST["description"]);
			$this->categoriesRepository->insert($category);
			include "Views/Categories/create.php";
		} else {
			header("Location: index.php?C=Categories&A=new");
		}
	}

}