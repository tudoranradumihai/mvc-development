<?php

Class UsersController extends Controller {

	private $usersRepository;

	public function __construct(){
		$this->usersRepository = new UsersRepository();
	}

	public function defaultAction(){

	}

	public function newAction(){
		include "Views/Users/new.php";
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$user = new Users();
			$user->setFirstname($_POST["firstname"]);
			$user->setLastname($_POST["lastname"]);
			$user->setEmailaddress($_POST["emailaddress"]);
			$user->setPassword($_POST["password"]);
			$result = $this->usersRepository->insert($user);
			include "Views/Users/create.php";
		} else {
			header("Location: index.php?C=Users&A=new");
		}
	}

	public function loginAction(){
		if(array_key_exists("userIdentifier", $_COOKIE)){
			header("Location: index.php?C=Users&A=welcome");	
		} else {
			include "Views/Users/login.php";	
		}
	}

	public function connectAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$user = $this->usersRepository->checkCredentials($_POST["emailaddress"],$_POST["password"]);
			if (!is_null($user)){
				setcookie("userIdentifier",$user->getId(),time()+3600);
				header("Location: index.php?C=Users&A=welcome");	
			} else {
				header("Location: index.php?C=Users&A=login");	
			}
		} else {
			header("Location: index.php?C=Users&A=login");
		}
	}

	public function logoutAction(){
		if(array_key_exists("userIdentifier", $_COOKIE)){
			setcookie("userIdentifier",NULL,time()-3600);
		}
		if(array_key_exists("HTTP_REFERER", $_SERVER)){
			header("Location: $_SERVER[HTTP_REFERER]");
		} else {
			header("Location: index.php");
		}
	}

	public function welcomeAction(){
		if(array_key_exists("userIdentifier", $_COOKIE)){
			$user = $this->usersRepository->findById($_COOKIE["userIdentifier"]);
			include "Views/Users/welcome.php";
		} else {
			header("Location: index.php?C=Users&A=login");	
		}
	}

	public function loginStatus(){
		if(array_key_exists("userIdentifier", $_COOKIE)){
			$usersRepository = new UsersRepository();
			$user = $usersRepository->findById($_COOKIE["userIdentifier"]);
		}
		include "Views/Users/loginStatus.php";
	}

	public static function administratorStatus(){
		$permission = false;
		if(array_key_exists("userIdentifier", $_COOKIE)){
			$usersRepository = new UsersRepository();
			$user = $usersRepository->findById($_COOKIE["userIdentifier"]);
			if($user->getAdministrator()=="1"){
				$permission = true;
			}
		}	
		return $permission;
	}

}