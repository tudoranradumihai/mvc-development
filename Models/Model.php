<?php

Abstract Class Model {

	protected $id;

	public function getProperties(){
		$reflection = new ReflectionClass($this);
		$reflectionProperties = $reflection->getProperties();
		$properties = array();
		foreach($reflectionProperties as $property){
			array_push($properties,$property->name);
		}
		return $properties;
	}

	public function getId(){
		return $this->id;
	}
	
	public function setId($id){
		$this->id = $id;
	}

/*
	// http://php.net/manual/ro/language.oop5.overloading.php#object.get
	public function __get($property){
		return $this->$property;
	}


	// http://php.net/manual/ro/language.oop5.overloading.php#object.set
	public function __set($property, $value){
		$this->property = $value;
	}
*/
	
}