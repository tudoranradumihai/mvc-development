<?php

Class Users extends Model {

	private $administrator;
	private $firstname;
	private $lastname;
	private $emailaddress;
	private $password;
	private $createddate;
	private $updateddate;

	public function getAdministrator(){
		return $this->administrator;
	}

	public function setAdministrator($administrator){
		$this->administrator = $administrator;
	}

	public function getFirstname(){
		return $this->firstname;
	}

	public function setFirstname($firstname){
		$this->firstname = $firstname;
	}

	public function getLastname(){
		return $this->lastname;
	}

	public function setLastname($lastname){
		$this->lastname = $lastname;
	}

	public function getEmailaddress(){
		return $this->emailaddress;
	}

	public function setEmailaddress($emailaddress){
		$this->emailaddress = $emailaddress;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setPassword($password){
		$this->password = $password;
	}

	public function getCreateddate(){
		return $this->createddate;
	}

	public function setCreateddate($createddate){
		$this->createddate = $createddate;
	}

	public function getUpdateddate(){
		return $this->updateddate;
	}
	
	public function setUpdateddate($updateddate){
		$this->updateddate = $updateddate;
	}
}