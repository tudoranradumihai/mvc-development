<?php

Abstract Class Repository {

	protected $database;
	protected $table;
	protected $model;

	public function __construct(){
		$this->database = new Database();
		$this->model = preg_replace('/Repository$/','',get_called_class());
		$this->table = strtolower(implode("_",preg_split('/(?=[A-Z])/', lcfirst(preg_replace('/Repository$/','',get_called_class())))));
	}

	public function findAll(){
		$query = "SELECT * FROM $this->table";
		$result = $this->database->query($query);
		if ($result->num_rows > 0){
			$array = array();
			while($row = $result->fetch_assoc()){
				$object = new $this->model();
				foreach($object->getProperties() as $value){
					$object->{"set".ucfirst($value)}($row[$value]);
				}
				array_push($array,$object);
			}
			return $array;
		} else {
			return NULL;
		}
	}

	public function findByID($id){
		$query = "SELECT * FROM $this->table WHERE id=$id";
		$result = $this->database->query($query);
		if ($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$object = new $this->model();
			foreach($object->getProperties() as $value){
				$object->{"set".ucfirst($value)}($row[$value]);
			}
			return $object;
		} else {
			return NULL;
		}
	}

	public function insert($object){
		$properties = array();
		$values = array();
		foreach($object->getProperties() as $property){
			if(!is_null($object->{'get'.ucfirst($property)}())){
				array_push($properties, $property);
				array_push($values, '"'.$object->{'get'.ucfirst($property)}().'"');
			}
		}
		$query = "INSERT INTO $this->table (".implode(",",$properties).") VALUES (".implode(",",$values).");";
		return $this->database->query($query);
	}

}