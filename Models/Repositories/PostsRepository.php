<?php

Class PostsRepository extends Repository {


	public function findAll(){
		$query = "SELECT * FROM posts ORDER BY createddate DESC";
		$result = $this->database->query($query);
		if ($result->num_rows > 0){
			$array = array();
			while($row = $result->fetch_assoc()){
				$object = new Posts();
				foreach($object->getProperties() as $value){
					$object->{"set".ucfirst($value)}($row[$value]);
				}
				array_push($array,$object);
			}
			return $array;
		} else {
			return NULL;
		}

	}

	public function findLastThree(){
		$query = "SELECT * FROM posts ORDER BY createddate DESC LIMIT 3";
		$result = $this->database->query($query);
		if ($result->num_rows > 0){
			$array = array();
			while($row = $result->fetch_assoc()){
				$object = new Posts();
				foreach($object->getProperties() as $value){
					$object->{"set".ucfirst($value)}($row[$value]);
				}
				array_push($array,$object);
			}
			return $array;
		} else {
			return NULL;
		}

	}

	public function findByCategory($category){
		$query = "SELECT * FROM posts WHERE category=$category ORDER BY createddate DESC";
		$result = $this->database->query($query);
		if ($result->num_rows > 0){
			$array = array();
			while($row = $result->fetch_assoc()){
				$object = new Posts();
				foreach($object->getProperties() as $value){
					$object->{"set".ucfirst($value)}($row[$value]);
				}
				array_push($array,$object);
			}
			return $array;
		} else {
			return NULL;
		}
	}
}