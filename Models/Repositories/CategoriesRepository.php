<?php

Class CategoriesRepository extends Repository {

	public function findAll(){
		$query = "SELECT * FROM categories ORDER BY name ASC";
		$result = $this->database->query($query);
		if ($result->num_rows > 0){
			$array = array();
			while($row = $result->fetch_assoc()){
				$object = new Categories();
				foreach($object->getProperties() as $value){
					$object->{"set".ucfirst($value)}($row[$value]);
				}
				array_push($array,$object);
			}
			return $array;
		} else {
			return NULL;
		}

	}

	public function findByID($id){
		$query = "SELECT * FROM categories WHERE id=$id";
		$result = $this->database->query($query);
		if ($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$object = new Categories();
			foreach($object->getProperties() as $value){
					$object->{"set".ucfirst($value)}($row[$value]);
				}
			return $object;
		} else {
			return NULL;
		}
	}
}