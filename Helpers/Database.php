<?php

Class Database {

	// http://php.net/manual/en/language.oop5.visibility.php
	private $connection;

	// http://php.net/manual/en/language.oop5.decon.php
	public function __construct(){
		// https://www.w3schools.com/php/php_mysql_connect.asp

		/*
		// http://php.net/manual/en/function.mysqli-connect.php
		$this->connection = mysqli_connect("localhost","root","","mvcdevelopment");
		*/

		$configuration = require "Configuration/Configuration.php";

		// http://php.net/manual/en/class.mysqli.php
		$this->connection = new mysqli(
			$configuration["DATABASE"]["HOSTNAME"],
			$configuration["DATABASE"]["USERNAME"],
			$configuration["DATABASE"]["PASSWORD"],
			$configuration["DATABASE"]["DATABASE"]
		);
	}

	public function query($query){
		/*
		// http://php.net/manual/ro/mysqli.query.php
		return mysqli_query($this->connection,$query);
		*/

		// http://php.net/manual/en/class.mysqli.php
		return $this->connection->query($query);
	}

	public function __destruct(){
		/*
		// http://php.net/manual/ro/mysqli.close.php
		mysqli_close($this->connection);
		*/

		// http://php.net/manual/en/class.mysqli.php
		$this->connection->close();
	}

}