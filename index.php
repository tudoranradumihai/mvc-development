<?php
session_start();
require "Helpers/Autoload.php";
?>
Menu: <a href="index.php">Home</a> |
<a href="index.php?C=Products&A=list">List Products</a> |
<a href="index.php?C=Posts&A=list">List Posts</a> |
<a href="index.php?C=Categories&A=list">List Categories</a> <br />
<?php
$status = new UsersController();
$status->loginStatus();
?>
<br />
Administrator Menu: <a href="index.php?C=Posts&A=new">New Post</a> | <a href="index.php?C=Categories&A=new">New Category</a>
<br />
<?php
/* 
MVC URL's
index.php - DefaultController::defaultAction()
index.php?C=Users - UsersController::defaultAction()
index.php?C=Users&A=list - UsersController::listAction()
*/

$configuration = require "Configuration/Configuration.php";

// http://php.net/manual/en/control-structures.if.php
// http://php.net/manual/en/function.array-key-exists.php
if (array_key_exists("C", $_GET)){
	$controller = $_GET["C"]."Controller";
} else {
	$controller = $configuration["GENERAL"]["HOMEPAGE"]["CONTROLLER"];
}

if (array_key_exists("A", $_GET)){
	$action = $_GET["A"]."Action";
} else {
	$action = $configuration["GENERAL"]["HOMEPAGE"]["ACTION"];
	/*
	Because we are initialising a default action if the URL does not specify one, we need to force all the controllers to have a defaultAction. To do this, all the controllers must be extended from a class that has an abstract method called defaultAction.
	*/
}

// http://php.net/manual/en/function.class-exists.php
if (class_exists($controller)){
	// http://php.net/manual/en/language.oop5.php
	$object = new $controller();
	// http://php.net/manual/en/function.method-exists.php
	if (method_exists($object, $action)){
		// http://php.net/manual/en/function.in-array.php
		if(in_array($controller."::".$action, $configuration["ADMINISTRATOR"])){
			$permission = UsersController::administratorStatus();
		} else {
			$permission = true;
		}

		if($permission){
			$object->$action();
		} else {
			echo "FORBIDDEN! You don't have the right to access this page.";
		}
	}
}